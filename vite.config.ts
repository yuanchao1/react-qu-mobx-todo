/*
 * @Author: yuanchao614 micheleyuan@163.com
 * @Date: 2023-05-04 12:55:54
 * @LastEditors: yuanchao614 micheleyuan@163.com
 * @LastEditTime: 2023-05-04 16:42:42
 * @FilePath: \react-qu-mobx-todo\vite.config.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    outDir: 'dist',
  }
})
